/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Node, Parser, HtmlRenderer, XmlRenderer } from "commonmark"

@Entry
@Component
struct Index {
  scroller: Scroller = new Scroller()
  @State renderResult: string = ""
  htmlResult: string = ''
  xmlResult: string = ''
  
  getResourceString(res: Resource) {
    return getContext().resourceManager.getStringSync(res.id)
  }

  async aboutToAppear() {
    let reader: Parser = new Parser();
    let htmlwriter: HtmlRenderer = new HtmlRenderer();
    let xmlwriter: XmlRenderer = new XmlRenderer();
    let parsed = reader.parse(
      "*" + this.getResourceString($r('app.string.Italicized_text')) + "*\n" +
        "**" + this.getResourceString($r('app.string.Bold_Text')) + "**\n" +
        "***" + this.getResourceString($r('app.string.Bold_italic_text')) + "***\n" +
        "<u>" + this.getResourceString($r('app.string.Underlined_text')) + "</u>\n" +
        "- " + this.getResourceString($r('app.string.The_first')) + "\n" +
        "- " + this.getResourceString($r('app.string.The_second')) + "\n" +
        "- " + this.getResourceString($r('app.string.The_third')) + "\n" +
        "- " + this.getResourceString($r('app.string.Chapter_2')) + "\n" +
        "	* " + this.getResourceString($r('app.string.Chapter_2_1')) + "\n" +
        "		+ " + this.getResourceString($r('app.string.Section_2_1_1')) + "\n" +
        "1. " + this.getResourceString($r('app.string.The_first')) + "\n" +
        "2. " + this.getResourceString($r('app.string.The_second')) + "\n" +
        "3. " + this.getResourceString($r('app.string.The_third')) + "\n" +
        "1. " + this.getResourceString($r('app.string.The_first')) + "：\n" +
        "    - " + this.getResourceString($r('app.string.The_first_item')) + "\n" +
        "    - " + this.getResourceString($r('app.string.The_second_item')) + "\n" +
        "2. " + this.getResourceString($r('app.string.The_second')) + "：\n" +
        "    - " + this.getResourceString($r('app.string.The_first_nested_item')) + "\n" +
        "    - " + this.getResourceString($r('app.string.The_second_nested_item')) + "\n" +
        "> " + this.getResourceString($r('app.string.block_quotations')) + "\n" +
        "> " + this.getResourceString($r('app.string.Rookie_Tutorial')) + "\n" +
        "> " + this.getResourceString($r('app.string.Learning_dreams')) + "\n" +
        "`printf()` " + this.getResourceString($r('app.string.function')) + "\n" +
        "```javascript\n" +
        "$(document).ready(function () {\n" +
        "    alert('runoob');\n" +
        "});\n" +
        "```\n" +
        "" + this.getResourceString($r('app.string.This_link')) + "(https://www.runoob.com)  \n" +
        "!" + this.getResourceString($r('app.string.Runoob_icon')) + "(http://static.runoob.com/images/runoob-logo.png)  \n" +
        "" + this.getResourceString($r('app.string.use')) + " <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Del</kbd> " + this.getResourceString($r('app.string.restart_computer')) + " \n "
    ); // parsed is a 'Node' tree
    this.htmlResult = htmlwriter.render(parsed);
    this.xmlResult = xmlwriter.render(parsed);
    this.renderResult = this.htmlResult

  }

  build() {
    Column() {
      Row() {
        Button('htmlRenderer').fontSize(px2fp(30)).width('40%').onClick(() => this.renderResult = this.htmlResult)
        Button('xmlRenderer').fontSize(px2fp(30)).width('40%').onClick(() => this.renderResult = this.xmlResult)
      }.margin(px2vp(20))

      Stack({ alignContent: Alignment.TopStart }) {
        Scroll(this.scroller) {
          Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
            Text(this.renderResult)
              .fontSize(20)
              .fontWeight(FontWeight.Bold)
          }
          .width('100%')
        }
        .scrollable(ScrollDirection.Vertical)
        .scrollBar(BarState.On)
        .scrollBarColor(Color.Gray)
        .scrollBarWidth(30)
        .onWillScroll((xOffset: number, yOffset: number) => {
          console.info(xOffset + ' ' + yOffset)
        })
        .onScrollEdge((side: Edge) => {
          console.info('To the edge')
        })
        .onScrollStop(() => {
          console.info('Scroll Stop')
        })
      }
      .width('100%')
      .height('100%')
      .backgroundColor(0xDCDCDC)
    }
  }
}