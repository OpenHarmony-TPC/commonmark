# CommonMark

## 简介
> Markdown是一种纯文本格式，用于编写结构化文档。CommonMark三方库用于将Markdown格式转换为Html或者xml，以便在网页中显示。本库是[CommonMark](https://github.com/commonmark/commonmark.js)在OpenHarmony中的使用示例。

## 下载安装
```
ohpm install commonmark
ohpm install @types/commonmark
```
OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用方法说明

```
var reader = new commonmark.Parser();
var writer = new commonmark.HtmlRenderer();
var parsed = reader.parse("Hello *world*"); // parsed is a 'Node' tree
// transform parsed if you like...
var result = writer.render(parsed); // result is a String
```

## 接口说明
```
var reader = new commonmark.Parser({smart: true});
var writer = new commonmark.HtmlRenderer({sourcepos: true});
```

`Parser`目前支持以下功能：
`smart`：如果为true，直引号将被卷起来，--将更改为破折号，---将更改为强破折号，和...将更改为椭圆。

`HtmlRenderer`和`XmlRenderer`（见下文）都支持以下选项：

sourcepos：如果为true，则块级元素的源位置信息将在data-sourcepos属性（HTML）或sourcepos属性（XML）中呈现。
safe：如果为true，则原始HTML将不会传递到HTML输出（将被注释取代），以及链接和图像中潜在不安全的URL（那些以javascript：、vbscript：、文件：开头的URL，除少数例外数据：）将被替换为空字符串。
softbreak：指定用于softbreak的原始字符串。
esc：指定用于转义字符串的函数。它的参数是字符串。

例如，要使软中断在HTML中呈现为硬中断，请执行以下操作：
`var writer = new commonmark.HtmlRenderer({softbreak: "<br />"});`

要使它们呈现为空白，请执行以下操作：
`var writer = new commonmark.HtmlRenderer({softbreak: " "});`

XmlRenderer作为HtmlRenderer的替代方案，并将生成AST的XML表示：
`var writer = new commonmark.XmlRenderer({sourcepos: true});`

解析器返回一个Node。定义了以下公共属性（那些标记为“只读”的属性只有getter，而不是setter）：
* type (read-only): a String, one of text, softbreak, linebreak, emph, strong, html_inline, link, image, code, document, paragraph, block_quote, item, list, heading, code_block, html_block, thematic_break.
* firstChild (read-only): a Node or null.
* lastChild (read-only): a Node or null.
* next (read-only): a Node or null.
* prev (read-only): a Node or null.
* parent (read-only): a Node or null.
* sourcepos (read-only): an Array with the following form: [[startline, startcolumn], [endline, endcolumn]].
* isContainer (read-only): true if the Node can contain other Nodes as children.
* literal: the literal String content of the node or null.
* destination: link or image destination (String) or null.
* title: link or image title (String) or null.
* info: fenced code block info string (String) or null.
* level: heading level (Number).
* listType: a String, either bullet or ordered.
* listTight: true if list is tight.
* listStart: a Number, the starting number of an ordered list.
* listDelimiter: a String, either ) or . for an ordered list.
* onEnter, onExit: Strings, used only for custom_block or custom_inline.

Node有以下公共方法：
* appendChild(child)：将节点子级附加到节点子级的末尾。
* prependChild(child)：将节点子项放在节点子项的开头。
* unlink()：从树中删除节点，切断其与同级和父级的链接，并根据需要关闭间隙。
* insertAfter(sibling)：在节点之后插入节点同级。
* insertBefore(sibling)：在节点之前插入节点同级。
* walker()：返回一个NodeWalker，该NodeWalker可用于迭代根植于节点的节点树。

walker()返回的NodeWalker有两种方法：

next()：返回属性输入的对象（布尔值，当我们从父级或同级输入节点时，该值为true，当我们从子级重新输入节点时，该值为false）。当我们完成遍历树时，返回null。
resumeAt(node, entering)：重置迭代器以在指定的节点恢复，并设置进入。（通常，除非对节点树进行破坏性更新，否则不需要此操作。）

这里是一个使用NodeWalker迭代树的示例，进行转换。此简单示例将所有文本节点的内容转换为所有大写字母：
```
let walker = parsed.walker();
let event, node;

while ((event = walker.next())) {
  node = event.node;
  if (event.entering && node.type === 'text') {
    node.literal = node.literal.toUpperCase();
  }
}
```
这个更复杂的示例将重点转换为所有大写字母：
```
let walker = parsed.walker();
let event, node;
let inEmph = false;

while ((event = walker.next())) {
  node = event.node;
  if (node.type === 'emph') {
    if (event.entering) {
      inEmph = true;
    } else {
      inEmph = false;
      // add Emph node's children as siblings
      while (node.firstChild) {
        node.insertBefore(node.firstChild);
      }
      // remove the empty Emph node
      node.unlink()
    }
  } else if (inEmph && node.type === 'text') {
      node.literal = node.literal.toUpperCase();
  }
}
```

## 约束与限制
在下述版本验证通过：

- DevEco Studio 版本： 4.1 Canary(4.1.3.317)

- OpenHarmony SDK:API11 (4.1.0.36)

## 目录结构
````
|---- CommonMarkOHOS  
|     |---- entry  # 示例代码文件夹
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-tpc/commonmark/issues)  给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-tpc/commonmark/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-tpc/commonmark/blob/master/LICENSE)  ，请自由地享受和参与开源。
